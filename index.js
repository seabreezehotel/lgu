// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBwSCyJ2yGs-9ejPVO_hDGYV_u-8p47EeA",
  authDomain: "lgu-7ec12.firebaseapp.com",
  projectId: "lgu-7ec12",
  storageBucket: "lgu-7ec12.appspot.com",
  messagingSenderId: "726616060260",
  appId: "1:726616060260:web:6ecdbb7cc4daec77fbca71"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  // Initialize variables
  const auth = firebase.auth()
  const database = firebase.database()
  
  // Set up our register function
  function register () {
    // Get all our input fields
    email = document.getElementById('email').value
    password = document.getElementById('password').value
    full_name = document.getElementById('full_name').value
    favourite_song = document.getElementById('favourite_song').value
    milk_before_cereal = document.getElementById('milk_before_cereal').value
  
    // Validate input fields
    if (validate_email(email) == false || validate_password(password) == false) {
      alert('Email or Password is Outta Line!!')
      return
      // Don't continue running the code
    }
    if (validate_field(full_name) == false || validate_field(favourite_song) == false || validate_field(milk_before_cereal) == false) {
      alert('One or More Extra Fields is Outta Line!!')
      return
    }
   
    // Move on with Auth
    auth.createUserWithEmailAndPassword(email, password)
    .then(function() {
      // Declare user variable
      var user = auth.currentUser
  
      // Add this user to Firebase Database
      var database_ref = database.ref()
  
      // Create User data
      var user_data = {
        email : email,
        full_name : full_name,
        favourite_song : favourite_song,
        milk_before_cereal : milk_before_cereal,
        last_login : Date.now()
      }
  
      // Push to Firebase Database
      database_ref.child('users/' + user.uid).set(user_data)
  
      // DOne
      alert('User Created!!')
    })
    .catch(function(error) {
      // Firebase will use this to alert of its errors
      var error_code = error.code
      var error_message = error.message
  
      alert(error_message)
    })
  }
  
  // Set up our login function
  function login () {
    // Get all our input fields
    email = document.getElementById('email').value
    password = document.getElementById('password').value
  
    // Validate input fields
    if (validate_email(email) == false || validate_password(password) == false) {
      alert('Email or Password is Outta Line!!')
      return
      // Don't continue running the code
    }
  
    auth.signInWithEmailAndPassword(email, password)
    .then(function() {
      // Declare user variable
      var user = auth.currentUser
  
      // Add this user to Firebase Database
      var database_ref = database.ref()
  
      // Create User data
      var user_data = {
        last_login : Date.now()
      }
  
      // Push to Firebase Database
      database_ref.child('users/' + user.uid).update(user_data)
  
      // DOne
      alert('User Logged In!!')
  
    })
    .catch(function(error) {
      // Firebase will use this to alert of its errors
      var error_code = error.code
      var error_message = error.message
  
      alert(error_message)
    })
  }
  // Function to fetch users and display them
function displayUsers() {
  var usersContainer = document.getElementById("users_container");

  // Clear previous content
  usersContainer.innerHTML = "";

  // Reference to the 'users' node in your database
  var usersRef = database.ref("users");

  // Fetch users data
  usersRef.once("value", function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
          var userData = childSnapshot.val();

          // Create HTML elements to display user data
          var userDiv = document.createElement("div");
          userDiv.innerHTML = "<p><strong>Name:</strong> " + userData.full_name + "</p>" +
                              "<p><strong>Email:</strong> " + userData.email + "</p>" +
                              "<p><strong>Favourite Song:</strong> " + userData.favourite_song + "</p>" +
                              "<p><strong>Milk Before Cereal:</strong> " + userData.milk_before_cereal + "</p>";

          // Append user data to the container
          usersContainer.appendChild(userDiv);
      });
  });
}

// Call the function to display users when the page loads
window.onload = function() {
  displayUsers();
};
  
  
  
  // Validate Functions
  function validate_email(email) {
    expression = /^[^@]+@\w+(\.\w+)+\w$/
    if (expression.test(email) == true) {
      // Email is good
      return true
    } else {
      // Email is not good
      return false
    }
  }
  
  function validate_password(password) {
    // Firebase only accepts lengths greater than 6
    if (password < 6) {
      return false
    } else {
      return true
    }
  }
  
  function validate_field(field) {
    if (field == null) {
      return false
    }
  
    if (field.length <= 0) {
      return false
    } else {
      return true
    }
  }

// Function to handle form submission
function submitForm(event) {
  event.preventDefault(); // Prevent default form submission

  // Get form values
  var name = document.getElementById('name').value;
  var email = document.getElementById('email').value;
  var concern = document.getElementById('concern').value;

  // Push to Firebase database
  var newSubmissionRef = database.ref('submissions').push();
  newSubmissionRef.set({
    name: name,
    email: email,
    concern: concern
  });

  // Clear form fields
  document.getElementById('name').value = '';
  document.getElementById('email').value = '';
  document.getElementById('concern').value = '';

  alert('Form submitted successfully!');
}

// Attach event listener to form submission
document.getElementById('contactForm').addEventListener('submit', submitForm);